﻿using System;
namespace ConsoleApp1
{
    internal class Dish
    {
        private string m_name;
        public string Name { get => m_name; set => m_name = value; }

        private Course m_course;
        public Course Course { get => m_course; set => m_course = value; }

        private int m_calories;
        public int Calories { get => m_calories; set => m_calories = value; }

        private int m_price;
        public int Price { get => m_price; set => m_price = value; }

        private bool m_vegan;
        public bool Vegan { get => m_vegan; set => m_vegan = value; }


        public Dish(string name, Course course, int calories, int price, bool vegan)
        {
            this.Name = name;
            this.Calories = calories;
            this.Course = course;
            this.Price = price;
            this.Vegan = vegan;

        }

        public override String ToString()
        {
            return Name;
        }
    }
}