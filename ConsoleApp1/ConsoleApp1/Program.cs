﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    delegate List<Dish> fillMenu();
    delegate bool selectDish(Dish dish);
    class Program
    {
        static void Main(string[] args)
        {
            Program prog = new Program();
            prog.Process();
        }

        public bool Rich(Dish dish)
        {
            if (dish.Price > 10)
                return true;
            else
                return false;
        }
        public bool Fatty(Dish dish)
        {
            if (dish.Calories > 200)
                return true;
            else
                return false;
        }

        public bool Vegan(Dish dish)
        {
            return dish.Vegan;
        }


        public bool Cheap(Dish dish)
        {
            if (dish.Price < 5)
                return true;
            else
                return false;
        }

        void Process()
        {
            IRestaurant FastnFat = new Restaurant(new fillMenu(FastFood));
            FastnFat.Open();
            IRestaurant SlownFat = new Restaurant(new fillMenu(SlowFood));
            SlownFat.Open();

            string result = FastnFat.ToString();
            Console.WriteLine("FastnFat's Menu: " + result);

            string result2 = SlownFat.ToString();
            Console.WriteLine("SlownFat's Menu: " + result2);

            Customer Jack = new Customer("Jack", new selectDish(Rich));
            SlownFat.Welcome(Jack);
            Customer Tao = new Customer("Tao", new selectDish(Fatty));
            FastnFat.Welcome(Tao);
            Customer Bill = new Customer("Bill", new selectDish(Vegan));
            SlownFat.Welcome(Bill);
            Customer Victor = new Customer("Victor", new selectDish(Cheap));
            FastnFat.Welcome(Victor);
            Console.ReadLine();
        }


        public List<Dish> FastFood()
        {
            List<Dish> Menu = new List<Dish>();
            Menu.Add(new Dish("Burger", Course.MAIN, 380, 15, false));
            Menu.Add(new Dish("Salad", Course.STARTER, 80, 8, true));
            Menu.Add(new Dish("ChocMousse", Course.DESSERT, 120, 5, false));
            Menu.Add(new Dish("Apple", Course.DESSERT, 12, 2, true));
            Menu.Add(new Dish("Okonomiaki", Course.MAIN, 300, 16, true));
            return Menu;
        }


        public List<Dish> SlowFood()
        {
            List<Dish> Menu = new List<Dish>();
            Menu.Add(new Dish("Shrimps", Course.MAIN, 180, 15, false));
            Menu.Add(new Dish("Mozza Sticks", Course.STARTER, 280, 8, true));
            Menu.Add(new Dish("Cheese Cake", Course.DESSERT, 170, 5, true));
            Menu.Add(new Dish("Ice Cream", Course.DESSERT, 62, 2, true));
            Menu.Add(new Dish("Sushis", Course.MAIN, 300, 150, false));
            return Menu;
        }

    }
}
