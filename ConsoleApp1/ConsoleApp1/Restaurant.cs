﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Restaurant : IRestaurant 
    {
        fillMenu leChef;
        private List<Dish> menu;

        public List<Dish> Menu { get => menu; set => menu = value; }

        public Restaurant(fillMenu leChef)
        {
            this.leChef = leChef;
        }

        public override String ToString()
        {
            string result = "\n";
            foreach (Dish element in this.Menu)
            {
                result += element.ToString() + "\n";
            }
            return result;
        }

        public void Open()
        {
            this.Menu = leChef();
        }

        public void Welcome(Customer customer)
        {
            Console.WriteLine("\n---Welcome " + customer.Name +"---");
            placeOrder(customer);
        }

        public void placeOrder(Customer customer)
        {
            Meal meal = new Meal();

            meal.bill = 0;
            meal.energy = 0;
            foreach (Dish element in this.Menu)
            {
                if (customer.preferences(element))
                {
                    Console.WriteLine("I'm having " + element.Name);
                    meal.bill += element.Price;
                    meal.energy += element.Calories;
                }
            }
            Console.WriteLine("Bill : " + meal.bill + " Calories: " + meal.energy);

        }
    }
}
